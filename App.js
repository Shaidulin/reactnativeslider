import React from 'react';
import { View } from 'react-native';
import SwapList from './components/SwipeList';

export default class App extends React.Component {
  render(): React.Node {
    return (
      <View>
        <SwapList />
      </View>
    );
  }
}
