{
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "es6": true,
    "jest": true
  },
  "parserOptions": {
    "ecmaFeatures": {
      "ecmaVersion": 6,
      "experimentalObjectRestSpread": true,
      "jsx": true
    },
    "sourceType": "module"
  },
  "extends": "airbnb",
  "plugins": [
    "react",
    "jsx-a11y",
    "import",
    "flowtype"
  ],
  "settings": {
    "import/resolver": {
      "webpack": {
        "config": "config/webpack.config.js"
      }
    },
    "flowtype": {
      "onlyFilesWithFlowAnnotation": false
    }
  },
  "rules": {
    "react/prefer-es6-class": [
      2,
      "always",
    ],
    "react/prefer-stateless-function": [0],
    "react/forbid-prop-types": [0],
    "react/jsx-no-bind": [0],
    "react/no-find-dom-node": [0],
    "react/require-default-props": [0],
    "import/no-extraneous-dependencies": [0],
    "import/extensions": [0],
    "import/prefer-default-export": [0],
    "class-methods-use-this": [0],
    "jsx-a11y/label-has-for": [0],
    "jsx-a11y/no-static-element-interactions": [0],
    "max-len": [2, 120, {"ignoreRegExpLiterals": true}],
    "no-restricted-syntax": [0],
    "no-underscore-dangle": [0],
    "react/jsx-filename-extension": [0],
    "jsx-a11y/href-no-hash": "off",
    "flowtype/boolean-style": [
      2,
      "boolean"
    ],
    "flowtype/define-flow-type": 1,
    "flowtype/delimiter-dangle": [
      2,
      "never"
    ],
    "flowtype/generic-spacing": [
      2,
      "never"
    ],
    "flowtype/no-primitive-constructor-types": 2,
    "flowtype/no-types-missing-file-annotation": 2,
    "flowtype/no-weak-types": 2,
    "flowtype/object-type-delimiter": [
      2,
      "comma"
    ],
    "flowtype/require-parameter-type": 2,
    "flowtype/require-return-type": [
      2,
      "always",
      {
        "annotateUndefined": "never"
      }
    ],
    "flowtype/require-valid-file-annotation": 2,
    "flowtype/semi": [
      2,
      "always"
    ],
    "flowtype/space-after-type-colon": [
      2,
      "always"
    ],
    "flowtype/space-before-generic-bracket": [
      2,
      "never"
    ],
    "flowtype/space-before-type-colon": [
      2,
      "never"
    ],
    "flowtype/type-id-match": [
      2,
      "^([A-Z][a-z0-9]+)+Type$"
    ],
    "flowtype/union-intersection-spacing": [
      2,
      "always"
    ],
    "flowtype/use-flow-type": 1,
    "flowtype/valid-syntax": 1
  },
  "globals": {
    "routes": true,
    "asset_path": true,
    "dataLayer": true
  }
}