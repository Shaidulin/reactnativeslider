export const setIndexElementToFirstElement = (arr:arr, i:number):arr => {
  const listAfterIndex = arr.slice(i);
  const listBeforeIndex = arr.slice(0, i);
  const listWithFirstIndexElement = [...listAfterIndex, ...listBeforeIndex];
  return listWithFirstIndexElement;
};

export const getPaddintTopToElementList = (elementIndex:number):any => {
  const styleObject = { paddingTop: 50 - 10 * elementIndex };
  return styleObject;
}