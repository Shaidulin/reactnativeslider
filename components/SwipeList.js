import React from 'react';
import {
  StyleSheet, View, Image, Animated, PanResponder,
} from 'react-native';
import { SCREEN_WIDTH, HEIGHT_CONTAINER } from './constants';
import { itemList } from './fixtures';
import { setIndexElementToFirstElement, getPaddintTopToElementList } from './utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    height: HEIGHT_CONTAINER,
    width: SCREEN_WIDTH,
    position: 'absolute',
  },
  image: {
    flex: 1,
    height: null,
    width: null,
    resizeMode: 'cover',
    borderRadius: 20,
  },
});

type State = {
    itemList: array,
    currentIndex: number
};


export default class SwipeList extends React.Component <any, State> {
  state = {
    currentIndex: 0,
    itemList,
  };

  position = new Animated.ValueXY();

  rotate = this.position.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: ['-10deg', '0deg', '20deg'],
    extrapolate: 'clamp',
  });

  rotateAndTranslate = {
    transform: [
      {
        rotate: this.rotate,
      },
      ...this.position.getTranslateTransform(),
    ],
  };

  componentWillMount() {
    const onStartShouldSetPanResponder = () => true;
    const onPanResponderMove = (evt: any, gestureState: any) => {
      this.position.setValue({ x: gestureState.dx, y: gestureState.dy });
    };
    const onPanResponderRelease = (evt: any, gestureState: any) => {
      const isGestureStateTurnRight = gestureState.dx > 120;
      const { currentIndex, itemList } = this.state;
      const indexToGo = isGestureStateTurnRight
        ? currentIndex + 1
        : currentIndex - 1;
      Animated.spring(this.position, {
        toValue: { x: SCREEN_WIDTH + 100, y: gestureState.dy },
      }).start(() => {
        this.setState({
          itemList: setIndexElementToFirstElement(itemList, indexToGo),
        });
        this.position.setValue({ x: 0, y: 0 });
      });
    };
    this.PanResponder = PanResponder.create({
      onStartShouldSetPanResponder,
      onPanResponderMove,
      onPanResponderRelease,
    });
  }

  getAnimatedContainerByIndex = (item: Array, elementIndex: Number) => {
    const { currentIndex } = this.state;
    const isIndexIsSameAsCurrentIndex = elementIndex === currentIndex;
    const paddingCurrentContainer = getPaddintTopToElementList(elementIndex);
    const animatedContainer = isIndexIsSameAsCurrentIndex
      ? this.rotateAndTranslate
      : {};
    const styleAnimatedContainer = Object.assign(
      {},
      styles.imageContainer,
      animatedContainer,
      paddingCurrentContainer,
    );

    return (
      <Animated.View
        {...this.PanResponder.panHandlers}
        key={item.id}
        style={styleAnimatedContainer}
      >
        <Image style={styles.image} source={item.uri} />
      </Animated.View>
    );
  };

  renderItemList = () => this.state.itemList.map(this.getAnimatedContainerByIndex).reverse();

  render(): React.Node {
    return <View style={styles.container}>{this.renderItemList()}</View>;
  }
}
